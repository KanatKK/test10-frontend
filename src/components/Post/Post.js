import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addAuthor, addId, addNewComment, addTxt, deleteComment, fetchComments, fetchPost} from "../../store/actions";
import Moment from "react-moment";

const Post = (props) => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.getById.newPost);
    const comments = useSelector(state => state.getC.comments);
    const addComment = useSelector(state => state.addC);

    useEffect(() => {
        dispatch(fetchPost(props.match.params.id));
        dispatch(fetchComments(props.match.params.id));
        dispatch(addId(props.match.params.id));
    }, [props.match.params.id, dispatch]);

    useEffect(() => {
        const interval = setInterval(() => {
            dispatch(fetchComments(props.match.params.id));
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch, props.match.params.id]);

    const addAuthorHandler = event => {
        dispatch(addAuthor(event.target.value));
    };

    const addTxtHandler = event => {
        dispatch(addTxt(event.target.value));
    };

    const delComment = (event) => {
        deleteComment(event.target.id);
    };

    let disabled = true;
    if (addComment.txt !== "") {
        disabled = false;
    }

    const sendComment = (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(addComment).forEach(key => {
            if (key !== 'imageName') {
                formData.append(key, addComment[key]);
            }
        });
        addNewComment(formData);
        dispatch(addAuthor(''));
        dispatch(addTxt(''));
    };

    if (post !== null) {
        return (
            <div className="container">
                <header><h3>News</h3></header>
                <div className="content">
                    <h2>{post.title}</h2>
                    <img className="postImage" src={'http://localhost:8000/uploads/' + post.image} alt=""/>
                    <p className="date">At <Moment>{post.postTime}</Moment></p>
                    <p className="news">{post.description}</p>
                    <div className="commentSection">
                        <h3>Comments</h3>
                        {
                            comments && comments.map(comment => {
                                if (comment.author === '') {
                                    return (
                                        <div key={comment.id} className="comment">
                                            <h4 className="author">Anonymous</h4>
                                            <p className="newComment">{comment.txt}</p>
                                            <button id={comment.id} type="button" className="deleteCommBtn" onClick={delComment}>
                                                Delete
                                            </button>
                                        </div>
                                    );
                                } else {
                                    return (
                                        <div key={comment.id} className="comment">
                                            <h4 className="author">{comment.author}</h4>
                                            <p className="newComment">{comment.txt}</p>
                                            <button id={comment.id} type="button" className="deleteCommBtn" onClick={delComment}>
                                                Delete
                                            </button>
                                        </div>
                                    );
                                }
                            })
                        }
                        <div className="addComment">
                            <input
                                type="text" className="authorsName" placeholder="Name"
                                onChange={addAuthorHandler} value={addComment.author}
                            />
                            <textarea
                                className="authorsComment" placeholder="Comment"
                                onChange={addTxtHandler} value={addComment.txt}
                            />
                            <button type="button" onClick={sendComment} disabled={disabled} className="addCommentBtn">Add</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    } else {
        return null
    }
};

export default Post;