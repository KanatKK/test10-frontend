import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addDate, addImage, addImageName, addNewPost, addNews, addTitle} from "../../store/actions";

const AddPost = props => {
    const title = useSelector(state => state.add.title);
    const news = useSelector(state => state.add.description);
    const imageName = useSelector(state => state.add.imageName);
    const addReducer = useSelector(state => state.add);
    const dispatch = useDispatch();

    const addTitleHandler = event => {
        dispatch(addTitle(event.target.value));
    };
    const addNewsHandler = event => {
        dispatch(addNews(event.target.value));
    };

    const addImageHandler = event => {
        if (event.target.files[0] !== undefined) {
            dispatch(addImageName(event.target.files[0].name));
            dispatch(addImage(event.target.files[0]));
        } else {
            dispatch(addImage(null));
            dispatch(addImageName("Choose an image..."));
        }
    };

    const getPostTime = () => {
        const dateNow = new Date();
        dispatch(addDate(dateNow.toISOString()));
    };

    const backToMainPage = () => {
        props.history.push('/');
    };

    const send = (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(addReducer).forEach(key => {
            if (key !== 'imageName') {
                formData.append(key, addReducer[key]);
            }
        });
        addNewPost(formData);
        dispatch(addTitle(''));
        dispatch(addNews(''));
        dispatch(addImage(null));
        dispatch(addImageName("Choose an image..."));
        dispatch(addDate(''));
        props.history.push('/');
    };

    let disabled = true;
    if (news !== '' && title !== '') {
        disabled = false;
    }
        return (
        <div className="container">
            <header>
                <h3>News</h3>
            </header>
            <div className="content">
                <h2 className="mainTitle">Add new post</h2>
                <form>
                    <input className="newsTitle" value={title} onChange={addTitleHandler} placeholder="Title" type="text"/>
                    <textarea className="newsPost" onBlur={getPostTime} value={news} onChange={addNewsHandler} placeholder="Content"/>
                    <p className="labelForImage">Choose image:</p>
                    <label className="inputFileLabel" htmlFor="inputFile">{imageName}</label>
                    <input onChange={addImageHandler} type="file" className="chooseImage" id="inputFile" accept=".jpg, .jpeg, .png"/>
                    <button className="savePost" disabled={disabled} onClick={send}>Save</button>
                </form>
                <button className="backButton" onClick={backToMainPage}>Back</button>
            </div>
        </div>
    );
};

export default AddPost;