import React, {useEffect} from 'react';
import {NavLink} from "react-router-dom";
import Moment from "react-moment";
import {deletePost, fetchPosts} from "../../store/actions";
import {useDispatch} from "react-redux";

const News = props => {
    const dispatch = useDispatch();
    const delPost = event => {
        deletePost(event.target.id);
    };

    useEffect(() => {
        const interval = setInterval(() => {
            dispatch(fetchPosts());
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch]);

    if (props.image !== 'null') {
        return (
            <div className="post">
                <div className="postImg">
                    <img className="img" src={'http://localhost:8000/uploads/' + props.image} alt=""/>
                </div>
                <div className="shortInfo">
                    <p className="postTitle">{props.title}</p>
                    <div className="postBottom">
                        <span className="date">At <Moment>{props.date}</Moment></span>
                            <button className="fullPostLink" value={props.id}>
                                <NavLink style={{
                                    color: '#0000FF',
                                    textDecoration: 'underline',
                                    border: 'none',
                                    background: 'none',
                                    fontSize: '15px',
                                }}  to={`/news/${props.id}`}>Read full post >></NavLink>
                            </button>
                        <button className="deletePostBtn" onClick={delPost} id={props.id}>Delete</button>
                    </div>
                </div>

            </div>
        );
    } else {
        return (
            <div className="post">
                <div className="shortInfo" style={{width: '100%'}}>
                    <p className="postTitle">{props.title}</p>
                    <div className="postBottom">
                        <span className="date">At {props.date}</span>
                        <button className="fullPostLink" value={props.id}>
                            <NavLink style={{
                                color: '#0000FF',
                                textDecoration: 'underline',
                                border: 'none',
                                background: 'none',
                                fontSize: '15px',
                            }}  to={`/news/${props.id}`}>Read full post >></NavLink>
                        </button>
                        <button className="deletePostBtn" onClick={delPost} id={props.id}>Delete</button>
                    </div>
                </div>
            </div>
        );
    }
};

export default News;