import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import MainPage from "../mainPage/mainPage";
import AddPost from "../../components/AddPost/AddPost";
import Post from "../../components/Post/Post";

const App = () => {
  return (
      <BrowserRouter>
          <Switch>
              <Route path="/" exact component={MainPage}/>
              <Route path="/addPost" component={AddPost}/>
              <Route path="/news/:id" component={Post}/>
          </Switch>
      </BrowserRouter>
  );
};

export default App;
