import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPosts} from "../../store/actions";
import News from "../../components/News/News";

const MainPage = props => {
    const addNewPost = () => {
        props.history.push('/addPost');
    };

    const dispatch = useDispatch();
    const allNews = useSelector(state => state.get.posts);

    useEffect(() => {
        const interval = setInterval(() => {
            dispatch(fetchPosts());
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch]);

    if (allNews) {
        let newsList = allNews.map((news, index) => {
            return (
                <News
                    key={index} title={news.title} date={news.date} image={news.image} id={news.id}
                />
            );
        });
        return (
            <div className="container">
                <header><h3>News</h3></header>
                <div className="content">
                    <h2 className="mainTitle">Posts</h2>
                    <button className="addPost" onClick={addNewPost}>Add new post</button>
                    <div className="posts">
                        {newsList}
                    </div>
                </div>
            </div>
        );
    } else {
        return (
            <div className="container">
                <header><h3>News</h3></header>
                <div className="content">
                    <h2 className="mainTitle">Posts</h2>
                    <button className="addPost" onClick={addNewPost}>Add new post</button>
                    <div className="posts"/>
                </div>
            </div>
        );
    }
};

export default MainPage;