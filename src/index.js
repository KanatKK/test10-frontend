import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, combineReducers} from "redux";
import thunkMiddleware from "redux-thunk";
import {Provider} from "react-redux";
import './index.css';
import App from './containers/app/App';
import * as serviceWorker from './serviceWorker';
import getPosts from "./store/reducers/getPosts";
import addPosts from "./store/reducers/addPosts";
import getPost from "./store/reducers/getPost";
import getComments from "./store/reducers/getComments";
import addComments from "./store/reducers/addComments";

const rootReducers = combineReducers({
    get: getPosts,
    add: addPosts,
    getById: getPost,
    getC: getComments,
    addC: addComments,
});

const store = createStore(rootReducers, applyMiddleware(thunkMiddleware));

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
