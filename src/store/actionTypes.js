export const GET_NEWS = 'GET_NEWS';

export const ADD_TITLE = 'ADD_TITLE';
export const ADD_NEWS = 'ADD_NEWS';
export const ADD_IMAGE = 'ADD_IMAGE';
export const ADD_DATE = 'ADD_DATE';
export const ADD_IMAGE_NAME = 'ADD_IMAGE_NAME';

export const GET_POST = 'GET_POST';

export const GET_COMMENTS = 'GET_COMMENTS';

export const ADD_AUTHOR = 'ADD_AUTHOR';
export const ADD_TXT = 'ADD_TXT';
export const ADD_POST_ID = 'ADD_POST_ID';