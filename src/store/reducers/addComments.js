import {ADD_AUTHOR, ADD_POST_ID, ADD_TXT} from "../actionTypes";

const initialState = {
    posts_id: null,
    author: '',
    txt: ''
};

const addComments = (state = initialState, action) => {
    switch (action.type) {
        case ADD_AUTHOR:
            return {...state, author: action.value};
        case ADD_TXT:
            return {...state, txt: action.value};
        case ADD_POST_ID:
            return {...state, posts_id: action.value};
        default:
            return state;
    }
};

export default addComments;