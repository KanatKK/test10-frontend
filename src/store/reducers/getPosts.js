import {GET_NEWS} from "../actionTypes";

const initialState = {
    posts: null,
};

const getPosts = (state = initialState, action) => {
    switch (action.type) {
        case GET_NEWS:
            return {...state, posts: action.value};
        default:
            return state;
    }
};

export default getPosts;