import {GET_COMMENTS} from "../actionTypes";

const initialState = {
    comments: null,
};

const getComments = (state = initialState, action) => {
    switch (action.type) {
        case GET_COMMENTS:
            return {...state, comments: action.value};
        default:
            return state;
    }
};

export default getComments;