import {ADD_DATE, ADD_IMAGE, ADD_IMAGE_NAME, ADD_NEWS, ADD_TITLE} from "../actionTypes";

const  initialState = {
    title: '',
    description: '',
    image: null,
    imageName: 'Choose an image...',
    postTime: '',
};

const addPosts = (state = initialState, action) => {
    switch (action.type) {
        case ADD_DATE:
            return {...state, postTime: action.value};
        case ADD_TITLE:
            return {...state, title: action.value};
        case ADD_NEWS:
            return {...state, description: action.value};
        case ADD_IMAGE:
            return {...state, image: action.value};
        case ADD_IMAGE_NAME:
            return {...state, imageName: action.value};
        default:
            return state;
    }
};

export default addPosts;