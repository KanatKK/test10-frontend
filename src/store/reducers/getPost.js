import {GET_POST} from "../actionTypes";

const initialState = {
    newPost: null,
};

const getPost = (state = initialState, action) => {
    switch (action.type) {
        case GET_POST:
            return {...state, newPost: action.value};
        default:
            return state;
    }
};

export default getPost;