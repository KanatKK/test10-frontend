import axios from 'axios';
import {
    ADD_AUTHOR,
    ADD_DATE,
    ADD_IMAGE, ADD_IMAGE_NAME,
    ADD_NEWS, ADD_POST_ID,
    ADD_TITLE,
    ADD_TXT,
    GET_COMMENTS,
    GET_NEWS,
    GET_POST
} from "./actionTypes";

export const getNews = value => {
    return {type: GET_NEWS, value};
};

export const addTitle = value => {
    return {type: ADD_TITLE, value};
};
export const addNews = value => {
    return {type: ADD_NEWS, value};
};
export const addImage = value => {
    return {type: ADD_IMAGE, value};
};
export const addDate = value => {
    return {type: ADD_DATE, value};
};
export const addImageName = value => {
    return {type: ADD_IMAGE_NAME, value};
};

export const getPostById = value => {
    return {type: GET_POST, value};
};

export const getComments = value => {
    return {type: GET_COMMENTS, value};
};

export const addAuthor = value => {
    return {type: ADD_AUTHOR, value};
};
export const addTxt = value => {
    return {type: ADD_TXT, value};
};
export const addId = value => {
return {type: ADD_POST_ID, value};
};

export const fetchPosts = () => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8000/posts');
            dispatch(getNews(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const addNewPost = (value) => {
    const sendPost = async () => {
        try {
            await axios.post('http://localhost:8000/posts', value);
        } catch (e) {
            console.log(e);
        }
    };
    sendPost().catch(e => {console.log(e)});
};

export const fetchPost = value => {
    return async dispatch => {
        const response = await axios.get('http://localhost:8000/posts/' + value);
        dispatch(getPostById(response.data));
    };
};

export const fetchComments = value => {
    return async dispatch => {
        const response = await axios.get('http://localhost:8000/comments/' + value);
        dispatch(getComments(response.data));
    };
};

export const addNewComment = value => {
    const sendComment = async () => {
        try {
            await axios.post('http://localhost:8000/comments/', value);
        } catch (e) {
            console.log(e);
        }
    };
    sendComment().catch(e => console.log(e));
};

export const deletePost = value => {
    const delPost = async () => {
        try {
            await axios.delete('http://localhost:8000/posts/' + value);
        } catch (e) {
            console.log(e);
        }
    };
    delPost().catch(e => {console.log(e)});
};

export const deleteComment = value => {
    const delComment = async () => {
        try {
            await axios.delete('http://localhost:8000/comments/' + value);
        } catch (e) {
            console.log(e);
        }
    };
    delComment().catch(e => {console.log(e)});
};